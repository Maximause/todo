<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once 'app/server.php';

//  session_start();

  if (!isset($_SESSION['user']['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['user']['username']);
  	header("location: login.php");
  }
?>

<?php
    $itemsQuery = mysqli_query($db,"
        SELECT id, name, done
        FROM items
        WHERE user = '". $_SESSION['user']['id'] . "'
        ORDER BY created ");

$items = mysqli_num_rows($itemsQuery)  ;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>To Do List</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/popup.js"></script>
</head>
<body>
    <div id="shadow">
        <div id="window">
            <input id="editing" type="text">
            <a onclick="document.getElementById('shadow').style.display = 'none';" class="close">Close</a>
            <a onclick="edit(this.dataset.id)" data-id="" class="complete"> Done</a>
        </div>
    </div>
    <div class="list">

        <!-- notification message -->
        <?php if (isset($_SESSION['success'])) : ?>
            <div class="error success" >
                <h3>
                    <?php
                        echo $_SESSION['success'];
                        unset($_SESSION['success']);
                    ?>
                </h3>
            </div>
        <?php endif ?>

        <!-- logged in user information -->
        <?php if(isset($_SESSION['user']["username"])) : ?>
            <h1 class="header">
                <p>Welcome <strong><?php echo $_SESSION['user']['username']; ?></strong></p>
                <p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
            </h1>
        <?php endif ?>

        <!-- To do list -->
        <?php if($items > 0 ): ?>
        <ul class="items">
            <?php while ($item = mysqli_fetch_array($itemsQuery)) { ?>
            <li>
                <span id="edit<?=$item['id'] ?>" class="item<?php echo $item['done'] ? ' done' : '' ?>"><?php echo $item['name']; ?></span>
                <?php if (!$item['done']): ?>
                    <a href="mark.php?as=done&item=<?php echo $item['id']; ?>" class="done-button">Mark as done</a>
                <?php endif; ?>

                <span class="item-delete"> </span>
                    <a href="mark.php?as=delete&item=<?php echo $item['id']; ?>" class="delete-button"> Delete</a>

                <span class="item-edit"> </span>
<!--                <a href="mark.php?as=edit&item=--><?php //echo $item['id']; ?><!--" class="edit-button"> Edit</a>-->
                <a onclick="showPopup(this.dataset.id)" data-id="<?=$item['id'] ?>" class="edit-button"> Edit</a>


            </li>
            <?php } ?>
        </ul>
        <?php else: ?>
            <p>You haven't added any items yet.</p>
        <?php endif; ?>

        <form class="item-add" action="create.php" method="post">
            <input type="text" name="name" placeholder="Type a new item here" class="input" autocomplete="off" required>
            <input type="submit" value="Add" class="submit">
        </form>
    </div>
</body>
</html>

